/*
* Resource Group 
*/

# Tipo String 
variable name_resource_group {
  default = "Falabella_recursos"
}
variable global_location{
  default = "eastus2"
}

/*
* Service Bus 
*/

#Tipo String  

variable namespace_sb{
  default = "Falabella"
}

variable sku{
  default = "Standard"
}
#Tipo List
variable list_topics{
    default = [ "coronarivus","piñera"]
}
#Tipo Mapa
variable map_suscription{
    default =  {
         coronavirus = "s1"
         coronavirus = "s2"
         coronavirus = "s3"
         piñera      = "s4"
         piñera      = "s5"
         piñera      = "s6"
    }
}





