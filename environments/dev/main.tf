module "resource_group"{
  source              = "../modules/resource_group"
  name_resource_group = var.name_resource_group
  global_location     = var.global_location 
}

module "service_bus" {
  source                                = "../modules/service_bus"
  name_resource_group                   = var.name_resource_group
  global_location                       = var.global_location 
  namespace_sb                          = var.namespace_sb
  sku                                   = var.sku
  list_topics                           = var.list_topics
  map_suscription                       = var.map_suscription
}

/*
module "key_vault"{
  source = "../../modules/resource-group"

}
*/