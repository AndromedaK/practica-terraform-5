locals {
  global_location      = var.global_location 
  name_resource_group  = var.name_resource_group
}

# Grupo de recurso
resource "azurerm_resource_group" "resource_group_sb" {
  name     = local.name_resource_group 
  location = local.global_location
}
