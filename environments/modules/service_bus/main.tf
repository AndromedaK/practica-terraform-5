
locals {
  global_location      = var.global_location 
  name_resource_group  = var.name_resource_group
  namespace_sb         = var.namespace_sb
}
# Namespace Service Bus
resource "azurerm_servicebus_namespace" "namespace_sb" {
  name                = local.namespace_sb
  location            = local.global_location
  resource_group_name = local.name_resource_group
  sku                 = var.sku

  tags = {
    source = "terraform"
  }
}
# Topico Service Bus
resource "azurerm_servicebus_topic" "topic_sb" {
  count               = length(var.list_topics)
  name                = var.list_topics[count.index]
  namespace_name      = local.namespace_sb
  resource_group_name = local.name_resource_group
  enable_partitioning = false
  #name                =  [ for topico in var.list_topics ] 
} 
# Suscripcion Service Bus
resource "azurerm_servicebus_subscription" "subscript_sb" {
  for_each                              = var.map_suscription
  name                                  = each.value
  resource_group_name                   = var.name_resource_group
  namespace_name                        = var.namespace_sb
  topic_name                            = each.key
  max_delivery_count                    = 10
  dead_lettering_on_message_expiration  = false
}
# Policy Writer
resource "azurerm_servicebus_topic_authorization_rule" "send_sb" {
  #for_each            = var.map_suscription
  #name                = format("%s-writer", each.value)
  count               = length(var.list_topics)
  name                = format("%s-writer",var.list_topics[count.index])
  namespace_name      = local.namespace_sb
  topic_name          = var.list_topics[count.index]
  resource_group_name = local.name_resource_group
  listen              = false
  send                = true
  manage              = false 
}
# Policy Reader
resource "azurerm_servicebus_topic_authorization_rule" "listen_sb" {
  #for_each            = var.map_suscription
  #name                = format("%s-reader", each.value)
  count               = length(var.list_topics)
  name                = format("%s-reader",var.list_topics[count.index])
  namespace_name      = local.namespace_sb
  topic_name          = var.list_topics[count.index]
  resource_group_name = local.name_resource_group
  listen              = true
  send                = false
  manage              = false
}
