 # Variables
variable global_location{
  type = string
}
variable name_resource_group{
  type = string
}

variable namespace_sb{
  type = string
}

variable sku{
  type = string
}

variable map_suscription{
  type = map
}

variable list_topics{
  type = list(string)
}



